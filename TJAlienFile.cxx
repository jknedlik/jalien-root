/*
 * TJAlienFile.cxx
 *
 *  Created on: Jul 18, 2014
 *      Author: Tatiana Tothova
 */

#include "TJAlienFile.h"
#include "TJAlienResult.h"
#include "TJAlien.h"
#include "TROOT.h"
#include "TObjString.h"
#include "TMap.h"
#include "TObjArray.h"
#include "TString.h"
#include "Rtypes.h"
#include "TSystem.h"
#include "TStopwatch.h"
#include "TVirtualMonitoring.h"
#include "TVirtualMutex.h"
#include "TProcessUUID.h"
#include "TUrl.h"
#include "TError.h"
#include <cstdlib>
#include <regex>

ClassImp(TJAlienFile)

TJAlienFile::TJAlienFile(const char *purl, Option_t *option,
        const char *ftitle, Int_t compress,
        Bool_t parallelopen, const char *lurl,
        const char *authz) :
        TXNetFile(purl, option, ftitle, compress, 0, parallelopen, lurl)    {
    TUrl logicalUrl(lurl);
    fLfn = logicalUrl.GetFile();
    fAuthz = authz;
    fGUID = "";
    fUrl = "";
    fPfn = "";
    fSE = "";
    fImage = 0;
    fNreplicas = 0;
    fOpenedAt = time(0);
}

//______________________________________________________________________________
TJAlienFile *TJAlienFile::Open(const char *url, Option_t *option,
        const char *ftitle, Int_t compress, Bool_t parallelopen)
{
    // Open a JAliEn remote file
    // @url JAliEn file path : alien://alice/test/test?<url_options>
    // @option READ/NEW/CREATE/RECREATE/UPDATE : UPDATE=NEW=RECREATE
    // A file can't be updated. If the content of a file is changed a new file is
    // created with the new content and the old one is deleted
    // - Ask for a list of envelopes
    // - Iterate through them until operation is successful or fail after trying
    // all envelopes

    if (gDebug > 1) ::Info("TJAlienFile::Open", "Trying to open file with URL = \"%s\"", url);

    // Check if JAliEn connection exists.
    if (!gGrid) {
        ::Error("TJAlienFile::Open", "No JAliEn Grid connection available!");
        return 0;
    }

    // Check if the connection is actually a JAliEn connection
    if((TString(gGrid->GetGrid())).CompareTo(TString("alien"))){
        ::Error("TJAlienFile::Open", "You don't have an active JAliEn grid connection!");
        return 0;
    }

    // Report this open phase as a temp one, since we have no object yet
    // To extra check, normally it should send a pointer to the current file
    // The information is just stored, not printed because of kFALSE
    if (gMonitoringWriter)
        gMonitoringWriter->SendFileOpenProgress(0, 0, "jalienopen", kFALSE);

    // ----------------------------------------------------------------------------------------
    // Parse input values - url, options
    //

    // Read the filename from the url
    TUrl inputUrl(url);
    TString fJName(inputUrl.GetFile());
    if (gDebug > 1) ::Info("TJAlienFile::Open", "File name: \"%s\"", fJName.Data());

    // Read options
    TString fJOption = option;
    fJOption.ToUpper();

    // There can be options appended to the url
    // E.g.: <url>?se=ALICE::CERN::EOS,!ALICE::SUBATECH::EOS,disk:2&publicaccess=1
    TString inputUrlOptions = inputUrl.GetOptions();
    if (gDebug > 1) ::Info("TJAlienFile::Open", "File URL options: \"%s\"", inputUrlOptions.Data());

    // Maybe there are SE options set in the environment
    TString storageElement = gSystem->Getenv("alien_CLOSE_SE");

    TMap *tags = new TMap();
    Bool_t publicAccess = kFALSE;

    // Check if the URL options contain the storage element and the public access info
    TObjArray *objOptions = inputUrlOptions.Tokenize("&");
    for (Int_t i = 0; i < objOptions->GetEntries(); i++) {
        TString loption = ((TObjString *) objOptions->At(i))->GetName();
        if (gDebug > 1) ::Info("TJAlienFile::Open", "Option \"%s\"", loption.Data());

        TObjArray *objTags = loption.Tokenize("=");
        if (objTags->GetEntries() == 2) {
            TString key = ((TObjString *) objTags->At(0))->GetName();
            TString value = ((TObjString *) objTags->At(1))->GetName();
            if (gDebug > 1) ::Info("TJAlienFile::Open", "Option \"%s\" = \"%s\"", key.Data(), value.Data());

            // Parse SE information
            if (!key.CompareTo("se", TString::kIgnoreCase))
            {
                storageElement = value;

                // Parse tags information (e.g., disk:2,tape:3)
                TObjArray *objSETags = storageElement.Tokenize(",");
                for (Int_t j = 0; j < objSETags->GetEntries(); j++) {
                    TString spec = ((TObjString *) objSETags->At(j))->GetName();
                    if (!spec.Contains("::") && spec.Contains(":")) {
                        TString key = TString(spec(0, spec.Index(":")));
                        TString value = TString(spec(spec.Index(":") + 1, spec.Length()));
                        tags->Add(new TObjString(key), new TObjString(value));
                        if (gDebug > 1) ::Info("TJAlienFile::Open", "Tag \"%s\" = \"%s\"", key.Data(), value.Data());
                    }
                }
            }

            // Parse publicaccess option
            if (!key.CompareTo("publicaccess", TString::kIgnoreCase)) {
                if (value.Atoi())
                    publicAccess = kTRUE;
            }
        }
        delete objTags;
    }
    delete objOptions;

    // If tags map is empty, add "disk:3" and force file to have 3 replicas
    TIter tagIterator(tags->GetTable());
    TPair *pair;
    if ((pair = (TPair *) tagIterator()) == NULL)
    {
        tags->Add(new TObjString("disk"), new TObjString("3"));
        if (storageElement != "")
            storageElement += ",";

        storageElement += "disk:3";
    }

    // TODO test 'access -p read' (means create file with public access in ROOT,
    // try to READ with this in url: 'publicAccess=1' and test TJAlienFile)

    // ----------------------------------------------------------------------------------------
    // Send command
    //

    Bool_t fJWritable = kFALSE;
    TObject *object = NULL;
    TIterator *iter = NULL;
    TJAlienResult *result = NULL;
    int imagenr = 0;    // image number

    // Iterate through the envelopes
    while (true)
    {
        imagenr++;

        // If out of envelopes, request a new one
        if (!object)
        {
            // ----------------------------------------------------------------------------------------
            // Construct the command to be sent
            //

            TString command("access");
            if (gDebug > 1) ::Info("TJAlienFile::Open", "Running access command =\"%s\"", command.Data());

            if (fJOption == "UPDATE" || fJOption == "RECREATE") {
                command += (" " + TString("write-version"));
                fJWritable = kTRUE;
            }
            else{
                if (fJOption == "NEW" || fJOption == "CREATE") {
                    command += (" " + TString("write-once"));
                    fJWritable = kTRUE;
                } else {
                    // if "READ" or nothing from previous matches - read is default
                    if (fJOption == "READ" && publicAccess) {
                        command += (" " + TString("-p") + " " + TString("read"));
                    } else {
                        command += (" " + TString("read"));
                    }
                }
            }

            command += (" " + fJName);

            if (storageElement != "") {
                command += " ";
                command += storageElement;
            }

            result = (TJAlienResult*) gGrid->Command(command.Data());
            if (!result)
            {
                ::Error("TJAlienFile::Open", "Failed to get a response from the server");
                return 0;
            }

            // Check exit code and result variables
            TObjString *errorMessage = 0;
            Int_t exitcode = ((TJAlien *) gGrid)->GetExitCode(result, errorMessage);

            if (exitcode != 0)
            {
                ::Error("Open", "%s", errorMessage->GetString().Data());
                delete result;
                return 0;
            }

            TList *list = dynamic_cast < TList * >(result);

            if (!list)
            {
                ::Error("TJAlienFile::Open", "Cannot get the access envelope for %s", fJName.Data());

                if (result)
                    delete result;

                // Reset the temp monitoring info
                if (gMonitoringWriter)
                    gMonitoringWriter->SendFileOpenProgress(0, 0, 0, kFALSE);

                return 0;
            }
            if (list->IsEmpty())
            {
                if (!fJWritable)
                    ::Error("TJAlienFile::Open", "The file %s does not exist", fJName.Data());
                else
                    ::Error("TJAlienFile::Open", "You don't have rights to open %s", fJName.Data());

                if (result)
                    delete result;

                return 0;
            }

            iter = list->MakeIterator();
            object = iter->Next();
        }

        // ----------------------------------------------------------------------------------------
        // Parse responce
        //

        Int_t nreplicas = 0;
        TString urlStr, seStr, sguid, pfnStr, authz, tagsStr;

        TMap *map = dynamic_cast < TMap * >(object);
        TObject *valueObject;   // Temp object to read values from map

        valueObject = map->GetValue("url");
        if (valueObject) urlStr = valueObject->GetName();

        valueObject = map->GetValue("se");
        if (valueObject) seStr = valueObject->GetName();

        valueObject = map->GetValue("nSEs");
        if (valueObject) nreplicas = ((TString) valueObject->GetName()).Atoi();

        valueObject = map->GetValue("guid");
        if (valueObject) sguid = valueObject->GetName();

        valueObject = map->GetValue("pfn");
        if (valueObject) pfnStr = valueObject->GetName();

        valueObject = map->GetValue("envelope");
        if (valueObject) authz = valueObject->GetName();

        valueObject = map->GetValue("tags");
        if (valueObject) tagsStr = valueObject->GetName();

        delete valueObject;

        if (gDebug > 1)
        {
            ::Info("TJAlienFile::Open", "%s", urlStr.Data());
            ::Info("TJAlienFile::Open", "%s", seStr.Data());
            ::Info("TJAlienFile::Open", "%d", nreplicas);
            ::Info("TJAlienFile::Open", "%s", sguid.Data());
            ::Info("TJAlienFile::Open", "%s", pfnStr.Data());
            ::Info("TJAlienFile::Open", "%s", tagsStr.Data());
        }

        // If envelope does not contain necessary information
        if ((!urlStr) || (!authz)) {
            if (fJWritable) {
                ::Error("TJAlienFile::Open", "Didn't get the authorization to write %s", fJName.Data());
            } else {
                ::Error("TJAlienFile::Open", "Didn't get the authorization to read %s from location %s", fJName.Data(), seStr.Data());
            }
            gGrid->Stdout();
            gGrid->Stderr();

            // Ban this SE
            if (storageElement != "")
                storageElement += ",";
            storageElement += "!";
            storageElement += seStr;

            // Try next location
            object = iter->Next();
            continue;
        }

        // ----------------------------------------------------------------------------------------
        // Form physical and logical URLs
        //

        // Extract the anchor from input url
        // alien:///alice/sim/2017/LHC17j8c/245353/001/aod_archive.zip#AliAOD.root
        //                                                             ^  anchor ^
        TString anchor = inputUrl.GetAnchor();

        // If no anchor was specified there, try to get it from server reply
        // root://grid03.hepl.hiroshima-u.ac.jp:1094//07/43168/80a25954-bfdb-11e7-9da3-bbe5aa2e0c61#AliAOD.root
        //                                                                                          ^  anchor ^
        if (anchor.Length() == 0)
        {
            TObjArray *tokens = urlStr.Tokenize("#");
            if (tokens->GetEntries() == 2)
            {
                anchor = ((TObjString *) tokens->At(1))->GetName();
                urlStr = ((TObjString *) tokens->At(0))->GetName();
            }
        }

        TUrl logicalUrl(url);
        TUrl physicalUrl(urlStr + TString("?&authz=") + authz);

        if (anchor.Length())
        {
            TString purl = physicalUrl.GetUrl();
            purl += "#";
            purl += anchor;
            physicalUrl.SetUrl(purl);
            TString lUrlOpt;
            lUrlOpt = "zip=";
            lUrlOpt += anchor;
            lUrlOpt += "&mkpath=1";
            logicalUrl.SetOptions(lUrlOpt);
            if (gDebug > 1) {
                ::Info("TJAlienFile::Open", "%s", logicalUrl.GetUrl());
                ::Info("TJAlienFile::Open", "%s", logicalUrl.GetFileAndOptions());
            }
        }
        else
        {
            TString lUrlOpt = logicalUrl.GetOptions();
            if (lUrlOpt.Length()) {
                lUrlOpt += "&mkpath=1";
                logicalUrl.SetOptions(lUrlOpt.Data());
            } else {
                logicalUrl.SetOptions("mkpath=1");
            }
        }

        // Add the original options from the alien URL
        if (inputUrlOptions) {
            physicalUrl.SetOptions(physicalUrl.GetOptions() + TString("&") + inputUrlOptions);
        }

        if (gDebug > 1)
        {
            ::Info("TJAlienFile::Open", "logicalUrl %s", logicalUrl.GetUrl());
            ::Info("TJAlienFile::Open", "physicalUrl %s", physicalUrl.GetUrl());
        }

        // ----------------------------------------------------------------------------------------
        // Actually open file
        //

        if (!fJWritable) {
            if (seStr != "")
                ::Info("TJAlienFile::Open", "Accessing file %s in SE <%s>", fJName.Data(), seStr.Data());
            else
                ::Info("TJAlienFile::Open", "Accessing file %s", fJName.Data());
        }

        TStopwatch timer;
        timer.Start();

        TJAlienFile *jalienfile =
                new TJAlienFile(physicalUrl.GetUrl(), fJOption, ftitle, compress,
                        parallelopen, logicalUrl.GetUrl(), authz);
        timer.Stop();

        // ----------------------------------------------------------------------------------------
        // Check its status
        //

        if (jalienfile->IsZombie())
        {
            // Bad status
            delete jalienfile;

            // Ban this SE
            if (storageElement != "")
                storageElement += ",";
            storageElement += "!";
            storageElement += seStr;

            // Try next location
            object = iter->Next();
            continue;
        } else {
            // Everything is OK, return handler
            jalienfile->SetSE(seStr);

            std::regex reg1("root://[a-z0-9-.]+:[0-9]+/");
            jalienfile->SetPfn(std::regex_replace(urlStr.Data(), reg1, "") + "#" + anchor);
            jalienfile->SetImage(imagenr);
            jalienfile->SetNreplicas(nreplicas);
            jalienfile->SetGUID(sguid);
            jalienfile->SetUrl(urlStr + "#" + anchor);
            jalienfile->SetElapsed(timer.RealTime());

            // We successfully wrote the file to the SE
            // So we need to write another n-1 replicas
            // Find a tag, that corresponds to this SE, and decrement it's value
            TIter tagIterator(tags->GetTable());
            TPair *pair;
            while ((pair = (TPair *) tagIterator())) {
                TObjString *keyStr = dynamic_cast < TObjString * >(pair->Key());
                TObjString *valueStr = dynamic_cast < TObjString * >(pair->Value());
                TString value = valueStr->GetName();
                int valueInt = value.Atoi();

                if (keyStr && valueStr) {
                    if (storageElement.Contains(keyStr->GetName()))
                    {
                        TString toReplace(keyStr->GetName());
                        toReplace += ":";
                        toReplace += valueStr->GetName();
                        TString replaceWith(keyStr->GetName());
                        replaceWith += ":";
                        replaceWith += valueInt - 1;

                        storageElement.ReplaceAll(toReplace, replaceWith);
                        break;
                    }
                }
            }

            jalienfile->SetPreferredSE(storageElement);

            delete result;
            return jalienfile;
        }

        object = iter->Next();
    }

    // ----------------------------------------------------------------------------------------
    // Access failed
    //

    if (fJWritable) {
        if (fJOption == "NEW" || fJOption == "CREATE") {
            ::Error("TJAlienFile::Open", "Could not create file %s", fJName.Data());
        } else {
            ::Error("TJAlienFile::Open", "Could not open file %s", fJName.Data());
        }
    } else {
        ::Error("TJAlienFile::Open", "Could not open any of the file images of %s", fJName.Data());
    }

    // Reset the temp monitoring info
    if (gMonitoringWriter)
        gMonitoringWriter->SendFileOpenProgress(0, 0, 0, kFALSE);

    delete result;
    return 0;
}

//______________________________________________________________________________
TJAlienFile::~TJAlienFile() {
    // TJAlienFile file dtor.

    if (IsOpen()) {
        Close();
    }
    if (gDebug)
        Info("~TJAlienFile", "dtor called for %s", GetName());

}

//______________________________________________________________________________
void TJAlienFile::Close(Option_t * option)
{
    UNUSED(option);
    if (!IsOpen()) return;

    // Close file
    TXNetFile::Close(option);

    // If READ operation we can exit
    if (fOption == "READ")
        return;

    // If WRITE operation we need to commit the LFN and PFN to the catalogue
    TString *command = new TString("commit");

    Long64_t siz = GetSize();
    if (siz <= 0)
        Error("Close", "the reported size of the written file is <= 0");

    TString sSize("");
    sSize += siz;
    TList *options = new TList();
    options->Add(new TObjString(fAuthz));
    options->Add(new TObjString(sSize));
    options->Add(new TObjString(fLfn));

    TJAlienResult *result = (TJAlienResult*) ((TJAlien *) gGrid)->RunJsonCommand(command, options);
    delete command;
    delete options;
    TObjString *errorMessage = 0;
    Int_t exitcode = ((TJAlien *) gGrid)->GetExitCode(result, errorMessage);
    if (exitcode != 0)
    {
        ::Error("Close", "%s", errorMessage->GetString().Data());
        delete result;
    }

    TJAlienResult *jalienResult = dynamic_cast < TJAlienResult * >(result);
    TList *list = dynamic_cast < TList * >(jalienResult);
    if (!list) {
        if (result) {
            delete result;
        }
        Error("Close", "cannot commit envelope for %s", fLfn.Data());
        gSystem->Unlink(fLfn);
    }

    if (result->GetKey(0, "lfn") != 0) {
        // The file has been committed
        // Create replicas
        Mirror();
    }
    else
    {
        Error("Close", "cannot register %s!", fLfn.Data());
        gSystem->Unlink(fLfn);
    }

    delete result;
}

//______________________________________________________________________________
void TJAlienFile::Mirror()
{
    // Send mirror command to create replicas
    TString *command = new TString("mirror");
    TList *options = new TList();
    options->Add(new TObjString(fLfn));

    // Number of replicas specified at the end of SE list
    options->Add(new TObjString("-S"));
    options->Add(new TObjString(fPrefSE));

    TJAlienResult *result = (TJAlienResult*) ((TJAlien *) gGrid)->RunJsonCommand(command, options);
    TObjString *errorMessage = 0;
    Int_t exitcode = ((TJAlien *) gGrid)->GetExitCode(result, errorMessage);
    if (exitcode != 0)
    {
        ::Error("Close", "%s", errorMessage->GetString().Data());
        delete result;
    }
    delete command;
    delete options;
    delete result;
}

//______________________________________________________________________________
TString TJAlienFile::SUrl(const char *lfn)
{
    // Get surl from lfn by asking AliEn catalog.

    TString command;
    TString surl;
    TString ch(CMDOPTIONS);

    if (!lfn) {
        return surl;
    }

    TUrl lurl(lfn);
    command = TString("access") + ch + TString("read") + ch + lurl.GetFile();

    TGridResult* result;

    if (!gGrid) {
        ::Error("TJAlienFile::SUrl","no grid connection");
        return surl;
    }

    result = gGrid->Command(command.Data(), kFALSE, TJAlien::kOUTPUT);
    if (!result) {
        ::Error("TJAlienFile::SUrl","couldn't get access URL for jalien file %s", lfn);
        return surl;
    }

    TIterator *iter = result->MakeIterator();
    TObject *object=0;
    TObjString *urlStr=0;

    object = iter->Next();
    if (object) {
        TMap *map = dynamic_cast < TMap * >(object);
        TObject *urlObject = map->GetValue("url");
        urlStr = dynamic_cast < TObjString * >(urlObject);

        if (urlStr) {
            surl = urlStr->GetName();
            delete object;
            return surl;
        }
    }

    ::Error("TJAlienFile::SUrl","couldn't get surl for jalien file %s", lfn);
    return surl;
}
