/*
 * TJAlienJobStatusList.cxx
 *
 *  Created on: Sep 4, 2014
 *      Author: Tatianka Tothova
 */

#include "TJAlienJobStatusList.h"
#include "TJAlienJobStatus.h"
#include "TROOT.h"

ClassImp(TJAlienJobStatusList);

void TJAlienJobStatusList::PrintCollectionEntry(TObject* entry, Option_t* /*option*/,
                                               Int_t /*recurse*/) const
{
    TJAlienJobStatus* jobstatus = (TJAlienJobStatus*) entry;
    TString split(jobstatus->GetKey("split"));
    TString queueid(jobstatus->GetKey("queueId"));
    TROOT::IndentLevel();
    printf("JobId = %s Split = %s\n", queueid.Data(), split.Data());
}
