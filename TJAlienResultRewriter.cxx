#include "TJAlienResultRewriter.h"

ClassImp(TJAlienResultRewriter);

using namespace std;

// Result codes definitions
// Map from command to pair of result codes (success, failure).
// A new key will be inserted into the result, this key being "__result__".
// The value of the "__result__" key is taken from this map, and it is a string.
// If the value string is empty (NULL or ""), then the "__result__" key is not
// inserted at all.
// Extend this map initializer when needed as new rewriting rules are discovered.
const map<string, pair<string, string>> TJAlienResultRewriter::rules = {
  {"ls"   , {"" , "0"}},
  {"cd"   , {"1", "0"}},
  {"stat" , {"1", "0"}},
  {"mkdir", {"1", "0"}},
};

void TJAlienResultRewriter::Rewrite(const string& command,
                                    TJAlienResult *inResult)
{
  auto cmdRule = rules.find(command);
  if (cmdRule != rules.end()) {
    ApplyRule(&(cmdRule->second), inResult);
  }
}

void TJAlienResultRewriter::ApplyRule(const std::pair<string, string>* rule,
                                      TJAlienResult* result)
{
  const char *code = NULL;

  if (IsSuccess(result) && rule->first != "") {
    code = rule->first.c_str();
  } else if(IsFailure(result) && rule->second != "") {
    code = rule->second.c_str();
  }

  if(code != NULL) {
    SetResultCode(result, code);
  }

}

void TJAlienResultRewriter::SetResultCode(TJAlienResult *result, const char *target) {
  TMap *m = ObtainFirstMap(result);
  RemoveResultCode(m);
  m->Add(new TObjString("__result__"), new TObjString(target));
}

TMap* TJAlienResultRewriter::ObtainFirstMap(TJAlienResult *result) {
  TMap *m;

  if(result->IsEmpty()) {
    m = new TMap();
    result->Add(m);
  } else {
    m = (TMap*)result->At(0);
  }

  return m;
}

void TJAlienResultRewriter::RemoveResultCode(TMap *m) {
  TPair *p = (TPair*)m->FindObject("__result__");
  if(p != NULL) {
    auto *obj = m->Remove(new TObjString("__result__"));
    delete obj;
  }
}

bool TJAlienResultRewriter::IsSuccess(TJAlienResult* result)
{
  auto key = TObjString("error");
  if(result && result->GetMetaData(&key)) {
    return false;
  } else {
    return true;
  }
}

bool TJAlienResultRewriter::IsFailure(TJAlienResult* result)
{
  return !(IsSuccess(result));
}
