#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class TJAlien;
#pragma link C++ class TJAlienCollection;
#pragma link C++ class TJAlienDirectory;
#pragma link C++ class TJAlienDirectoryEntry;
#pragma link C++ class TJAlienFile;
#pragma link C++ class TJAlienJDL;
#pragma link C++ class TJAlienJob;
#pragma link C++ class TJAlienJobStatus;
#pragma link C++ class TJAlienJobStatusList;
#pragma link C++ class TJAlienMasterJob;
#pragma link C++ class TJAlienMasterJobStatus;
#pragma link C++ class TJAlienPackage;
#pragma link C++ class TJAlienResult;
#pragma link C++ class TJAlienResultRewriter;
#pragma link C++ class TJAlienSAXHandler;
#pragma link C++ class TJAlienSystem;
#endif
