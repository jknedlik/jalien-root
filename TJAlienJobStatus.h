/*
 * TJAlienJobStatus.h
 *
 *  Created on: Sep 4, 2014
 *      Author: Tatianka Tothova
 */

#ifndef ROOT_TJAlienJobStatus
#define ROOT_TJAlienJobStatus

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienJobStatus                                                      //
//                                                                      //
// JAliEn implementation of TGridJobStatus.                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifndef ROOT_TGridJobStatus
#include "TGridJobStatus.h"
#endif
#ifndef ROOT_TMap
#include "TMap.h"
#endif

class TJAlienJob;
class TJAlienMasterJob;

class TJAlienJobStatus : public TGridJobStatus {

friend class TJAlienJob;
friend class TJAlienMasterJob;

private:
    TMap fStatus;     // Contains the status information of the job.
                      // In the Alien implementation this is a string, string map.
    TString fJdlTag;  // JdlTag

public:
    TJAlienJobStatus() {};
    TJAlienJobStatus(TMap *status);
    virtual ~TJAlienJobStatus();

    const char *GetJdlKey(const char *key);
    const char *GetKey(const char *key);

    virtual TGridJobStatus::EGridJobStatus GetStatus() const;
    virtual void Print(Option_t *) const;
    virtual void PrintJob(Bool_t full) const;
    
    Bool_t IsFolder() const { return kTRUE;}
    void Browse(TBrowser *b);

    ClassDef(TJAlienJobStatus,1)  // JAliEn implementation of TGridJobStatus
};

#endif /* ROOT_TJAlienJobStatus */
