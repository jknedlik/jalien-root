JAliEn implementation of the TGrid interface
============================================

This is the JAliEn client version for ROOT. It compiles as a ROOT plugin and
handles `alien:` Grid connections and `alien://` files.


Installation
------------

The installation procedure is based on aliBuild and uses ROOT 6. Please refer to
[this guide](https://alice-doc.github.io/alice-analysis-tutorial/building/) for
base aliBuild instructions.

Before you start, you must get the build recipes from the experimental
repository:

```bash
rm -rf alidist  # make a backup first if you wish
git clone https://github.com/dberzano/alidist -b jalien-integration
```

Since you are going to develop JAliEn-ROOT, have it ready as "development
package" as seen by aliBuild:

```bash
aliBuild init JAliEn-ROOT
```

The project will be cloned as a Git repository, make sure you have all the
proper "remotes" set correctly.

Now follow the steps from the guide above, but build the JAliEn plugin instead
of AliRoot/AliPhysics:

```bash
aliBuild build JAliEn-ROOT --defaults user-root6
```

**In the current directory, from where you'll launch ROOT**, create a `.rootrc`
file with the following content:

```
Unix.*.Root.PluginPath: $(ROOTSYS)/etc/plugins:$(JALIEN_ROOT_ROOT)/etc/plugins
```

For the moment, [this is a
workaround](https://root.cern.ch/root/htmldoc/guides/users-guide/GettingStarted.html#environment-setup)
to make ROOT see our plugin without modifying any configuration file.

To use the plugin there is no need to load the environment permanently, it is
sufficient to load ROOT within the proper environment:

```bash
alienv setenv JAliEn-ROOT/latest -c root -l -b
```
