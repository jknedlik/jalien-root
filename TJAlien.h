// @(#)root/net:$Id$
// Author: Fons Rademakers   3/1/2002

/*************************************************************************
 * Copyright (C) 1995-2002, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_TJAlien
#define ROOT_TJAlien
#include <stdio.h>
#include <unistd.h>
#include <map>
#include <fstream>

#ifdef _DEBUG
#define DEBUGMSG(msg) ( (void) (std::cerr<<msg<<std::endl) )
#else
#define DEBUGMSG(msg) ( (void) 0 )
#endif
#define CMDEND '\0'
#define CMDOPTIONS '\1'

#ifndef ROOT_TGrid
#include "TGrid.h"
#endif

#ifndef ROOT_TList
#include "TList.h"
#endif

#include "TArrayI.h"
#include "THashList.h"
#include "TJAlienSAXHandler.h"
#include "TJAlienResult.h"
#include <TLockFile.h>

#ifndef __CINT__
#include <curl/curl.h>
#include <json-c/json.h>
#else
struct json_object;
#endif

#ifdef __CINT__
#undef __GNUC__
typedef char __signed;
typedef char int8_t;
#endif
//#include "picojson.h"
//#include "json.hpp"
//#include "rapidjson/document.h"
//#include <cstdint>

//#include <jsoncons/json.hpp>
//#include <tao/json.hpp>

#include <stdlib.h>
#include <string>
#include <signal.h>
#if !defined(__CINT__) && !defined(__MAKECINT__)
#include <libwebsockets.h>
#include "lws_config.h"
#endif

#if (SSLEAY_VERSION_NUMBER >= 0x0907000L)
#include <openssl/conf.h>
#include <openssl/ssl.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/lhash.h>
#include <openssl/buffer.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#endif

#define UNUSED(x) (void)(x)

class TJAlien : public TGrid {

public:
    enum { kSTDOUT = 0, kSTDERR = 1 , kOUTPUT = 2, kENVIR = 3 };
    enum CatalogType { kFailed = -1, kFile = 0, kDirectory, kCollection };

private:
    TString fPwd;        // working directory
    TString fHome;       // home directory with alien:// prefix
    int     fWSPort;     // websocket port
    TString sUsercert;   // location of user certificate
    TString sUserkey;    // location of user private key
    static std::string readBuffer;
    std::string homedir; // local home directory
    std::string tmpdir;  // tmp directory

    const std::string default_server = "alice-jcentral.cern.ch";
    Bool_t ReadJClientFile(const char* jclientFilePath);
    Bool_t WriteTokenFile();
    static size_t WriteCallback(void *contents, size_t size, size_t nmemb);

    void CreateConnection();
    void ConnectJBox(std::string certpath, std::string keypath);
    void MakeWebsocketConnection(std::string certpath, std::string keypath);

    // Format command to Json structure
    json_object *CreateJsonCommand(TString *command, TList *options);

    // Parse the result from Json structure
    TJAlienResult *GetCommandResult(json_object *json_response);

    // Libwebsockets
    struct lws_context_creation_info creation_info;	// Info to create logical connection
    struct lws_context *context;            // Context contains all information about connection
    struct lws *wsi;                        // WebSocket Instance - real connection object, created basing on context

    static int destroy_flag;                // Flags to know connection status
    static int connection_flag;
    static int writeable_flag;
    static int receive_flag;

    struct session_data {                   // Needed by protocol, TODO: clarify if can be replaced by just int var
        int fd;
    };

    static int ws_service_callback(         // Callback to handle connection
        struct lws *wsi,
        enum lws_callback_reasons reason, void *user,
        void *in, size_t len);
    // lws_write helper
    static int websocket_write_back(struct lws *wsi_in, const char *str, int str_size_in);

    virtual TGridResult *OpenDataset(const char *lfn, const char *options = "");
    const char* Whoami();

public:
    TJAlien(const char *gridUrl, const char *uId=0, const char *passwd=0,
            const char *options=0);
    virtual ~TJAlien();


    TJAlienResult *RunJsonCommand(TString *command, TList *options);
    TGridResult *Command(const char * command, bool interactive = kFALSE, UInt_t stream = kOUTPUT);

    Int_t GetExitCode(TJAlienResult *result, TObjString* &message);
    unsigned int ReadTags(int column, std::map<std::string, std::string> &tags) const;

    virtual TGridResult *Query(const char *path, const char *pattern, const char *conditions = "", const char *options = "");


    void Stdout();          // print the stdout of the last executed command
    void Stderr();          // print the stderr of the last executed command

    void Token(Option_t* options = "", bool force_restart = true);
    //--- Catalogue Interface
    virtual TGridResult *Ls(const char *ldn = "", Option_t *options = "", Bool_t verbose = kFALSE);
    virtual Bool_t Cd(const char* ldn = "",Bool_t verbose = kFALSE);
    virtual const char  *Pwd(Bool_t verbose = kFALSE);
    virtual const char  *GetHomeDirectory() { return fHome.Data(); }
    virtual Int_t  Mkdir(const char* ldn = "", Option_t* options = "", Bool_t verbose = kFALSE);
    virtual Bool_t Rmdir(const char* ldn = "", Option_t* options = "", Bool_t verbose = kFALSE);
    virtual Bool_t Register(const char *lfn, const char *turl, Long_t size=-1, const char *se=0, const char *guid=0, Bool_t verbose=kFALSE);
    virtual Bool_t Rm(const char *lfn, Option_t *option = "", Bool_t verbose = kFALSE);
    virtual TJAlien::CatalogType Type(const char* lfn, Option_t* option = "", Bool_t verbose = kFALSE);
    TGridResult* GetCollection(const char* lfn, Option_t* option = "", Bool_t verbose = kFALSE);

    //--- Job Submission Interface
    virtual TGridJob *Submit(const char * jdl);
    virtual TGridJDL *GetJDLGenerator();        // get a JAliEn grid JDL object
    virtual Bool_t ResubmitById(TString jobid);
    virtual Bool_t KillById(TString jobid);

    virtual TGridJobStatusList *Ps(const char* options = "", Bool_t verbose = kFALSE);
    virtual TGridCollection *OpenCollection(const char *, UInt_t maxentries = 1000000);
    virtual TGridCollection *OpenCollectionQuery(TGridResult *queryresult, Bool_t nogrouping);

    //--- Not implemented staff here
    virtual void NotImplemented(const char *func, const char *file, int line);
    TMap *GetColumn(UInt_t stream = 0, UInt_t column = 0);
    const char *GetStreamFieldValue(UInt_t stream, UInt_t column, UInt_t row);
    const char *GetStreamFieldKey(UInt_t stream, UInt_t column, UInt_t row);
    UInt_t GetNColumns(UInt_t stream);
    virtual TGridResult* ListPackages(const char* alienpackagedir="/alice/packages");

    ClassDef(TJAlien, 0)  // Interface to JAliEn GRID services
};
#endif
